# SENTINEL #
## The Application Lifeguard ##


![icon](./icon.png)

Launch an Application without a screen loader, check for updates and keep watching for events.

Features:

- Application Launcher
- Scheduled Updates
- Scheduled Restarts
- Scheduled Custom Actions
- Send notifications (email, sms)

## Binaries ##

* Mac: Mac binaries are [here](_build/mac).
* Windows: Windows binaries are [here](_build/windows).
* Linux: Linux binaries are [here](_build/linux).
* Raspberry binaries are [here](_build/linux_raspberry_3).

## Why did I write SENTINEL ##

Because I needed something able to monitor over my applications and eventually take an action to rescue from trouble.

![icon](./sentinel.png)

SENTINEL launch and monitor your application in a container perfectly integrated in host operating system.

## Configuration ##

SENTINEL is fully configurable changing configuration file.

```
{
   "version_file": "https://gianangelogeminiani.me/download/version.txt",
    "package_files": [
        {
          "file": "https://gianangelogeminiani.me/download/package.zip",
          "target": "$dir_home/_download"
        }
    ],
    "command_to_run": "open $dir_home/Mypp.app",
    "keep_alive":true,
    "scheduled_updates": [
        {
          "uid": "every_3_seconds",
          "start_at": "",
          "timeline": "second:3"
        }
    ],
    "scheduled_restart": []
    
}
```

The configuration file is stored into "workspace". Default workspace dir is "./_update_workspace", but can be changed by
passing a path to "launcher" executable.

For example: `launcher -dir_work=./new_workspace_path`

**Parameters**

* **version_file**: Path (relative or absolute) to background image. If not specified is used a default image. The
  screen loader size is same as image size (default image size is 600x313).
* **package_files**: Array of objects (PackageFile) to download and unzip (if archive). PackageFile contains "file"
  and "target" fields.
* **command_to_run**: Command to run when screen launcher is active. Use this to run your program.

**Variables**

Some parameters (`command_to_run`) can contain variables.

* $time: Is replaced with current time (hour:minute seconds)
* $datetime: Is replaced with current date and time.
* dir_start: Is replaced with Application start path.
* dir_app: Is replaced with Application binary path.
* dir_work: Is replaced with Application Workspace.
* dir_home: Is replaced with Application Workspace parent path.

## SAMPLE CONFIGURATION ##

```json
{
  "version_file": "http://localhost/downloads/runtime/version.txt",
  "package_files": [
    {
      "file": "http://localhost/downloads/runtime/guardian",
      "target": "$dir_app/_guardian/"
    },
    {
      "file": "http://localhost/downloads/runtime/settings.production.json",
      "target": "$dir_start/_guardian/"
    }
  ],
  "command_to_run": "$dir_app/guardian run -m=production -dir_work=$dir_start/_guardian -dir_start=$dir_start -dir_app=$dir_app",
  "keep_alive":true,
  "scheduled_updates": [
    {
      "uid": "every_10_seconds",
      "start_at": "",
      "timeline": "second:10"
    }
  ],
  "scheduled_restart": [
    {
      "uid": "every_12_hours",
      "start_at": "06:00",
      "timeline": "hour:12"
    }
  ],
  "scheduled_tasks": [
    {
      "uid": "every_10_seconds",
      "start_at": "",
      "timeline": "second:10",
      "payload": {
        "event": "file-changed",
        "action": "restart",
        "file": "/var/www/certificates/certificate.pem"
      }
    }
  ],
  "notifications": {
    "email": {
      "host": "pro.turbo-smtp.com",
      "port": 465,
      "secure": true,
      "auth": {
        "user": "xxx",
        "pass": "xxx"
      },
      "from": "User <info@foo.com>"
    },
    "sms": {
      "to": "+39",
      "from": "A Test",
      "driver": "skebby",
      "params": {
        "username": "",
        "password": "",
        "quality": "SI",
        "from": "TEST-SRV"
      }
    }
  },
  "stop_cmd": "stop"
}
```

## Sample: How to restart Application when a file changes ##

Configuration below show you:

 - `"command_to_run"` : SENTINEL start an application called "guardian"
 - `"keep_alive"` : SENTINEL check for exec crash and try to re-launch the executable keeping it alive
 - `"scheduled_tasks"` : SENTINEL schedule a task every 10 seconds to check is a file is changed. Once file change, 
an action is performed ("restart")
 - `"notification"` : Notifications are enabled and a message was sent using EMAIl and SMS.

```json
{
  "version_file": "",
  "package_files": [],
  "command_to_run": "$dir_app/guardian run -m=production -dir_work=$dir_start/_guardian -dir_start=$dir_start -dir_app=$dir_app",
  "keep_alive": true,
  "scheduled_updates": [],
  "scheduled_restart": [],
  "scheduled_tasks": [
    {
      "uid": "every_10_seconds",
      "start_at": "",
      "timeline": "second:10",
      "payload": {
        "event": "file-changed",
        "action": "restart",
        "file": "/var/www/certificates/certificate.pem"
      }
    }
  ],
  "notifications": {
    "email": {
      "host": "pro.turbo-smtp.com",
      "port": 465,
      "secure": true,
      "auth": {
        "user": "xxx",
        "pass": "xxx"
      },
      "from": "User <info@foo.com>"
    },
    "sms": {
      "to": "+39",
      "from": "A Test",
      "driver": "skebby",
      "params": {
        "username": "",
        "password": "",
        "quality": "SI",
        "from": "TEST-SRV"
      }
    }
  },
  "stop_cmd": "stop"
}
```

## BINARIES ##

Binaries are [here](https://mega.nz/folder/3wIBVLpD#l11i3m-r41Dg0EqAvYKjEw).