##!/bin/sh

NAME="sentinel"
BASE="0.2"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

# linux
echo "  * LINUX (amd64)"
fyne-cross linux -arch=amd64 -name=$NAME
echo "    copy executables ..."
cp ./fyne-cross/bin/linux-amd64/$NAME ./__build/linux-amd64/$NAME

echo "remove temp files ..."
rm -r ./fyne-cross