package main

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-progr-sentinel/sentinel"
	"bitbucket.org/digi-sense/gg-progr-sentinel/sentinel/commons"
	"flag"
	"fmt"
	"path/filepath"
	"time"
)

var logger gg_log.ILogger

//----------------------------------------------------------------------------------------------------------------------
//	l a u n c h e r
//----------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := gg.Strings.Format("[panic] APPLICATION %s ERROR: %s. Workspace: %s",
				commons.Name, r, gg.Paths.GetWorkspacePath())
			if nil != logger {
				logger.Error(message)
				time.Sleep(3 * time.Second) // wait logger can write
			} else {
				fmt.Println(message)
			}
		}
	}()

	// flags
	dirStart := flag.String("dir_start", "", "Set a particular folder as launch directory")
	dirApp := flag.String("dir_app", "", "Set a particular folder as app directory")
	dirWork := flag.String("dir_work", "./_update_workspace", "Set a particular folder as main workspace")
	mode := flag.String("m", commons.ModeProduction, "Mode allowed: 'debug' or 'production'")
	flag.Parse()

	program, err := sentinel.NewAppLauncher(*dirStart, *dirApp, *dirWork, *mode)

	if nil == err {
		// get logger from guardian
		logger = program.GetLogger()
		logger.Info(gg.Strings.Format("APPLICATION %s v.%s", commons.Name, commons.Version))
		logger.Info(gg.Strings.Format("DIR_START: %s", gg.Paths.GetWorkspace(commons.DirStart).GetPath()))
		logger.Info(gg.Strings.Format("DIR_APP: %s", gg.Paths.GetWorkspace(commons.DirApp).GetPath()))
		logger.Info(gg.Strings.Format("DIR_WORK: %s", gg.Paths.GetWorkspacePath()))
		logger.Info(gg.Strings.Format("DIR_ROOT: %s", filepath.Dir(gg.Paths.GetWorkspacePath())))
		err = program.Start()
		if nil != err {
			panic(err)
		}

		_ = program.Wait()
	} else {
		panic(err)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
