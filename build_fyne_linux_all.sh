##!/bin/sh

NAME="sentinel"
BASE="0.2"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

# linux
echo "  * LINUX (amd64,386,arm,arm64)"
fyne-cross linux -arch=amd64,386,arm,arm64 -name=$NAME
echo "    copy executables ..."
cp ./fyne-cross/bin/linux-386/$NAME ./__build/linux-386/$NAME
cp ./fyne-cross/bin/linux-amd64/$NAME ./__build/linux-amd64/$NAME
cp ./fyne-cross/bin/linux-arm/$NAME ./__build/linux-arm/$NAME
cp ./fyne-cross/bin/linux-arm64/$NAME ./__build/linux-arm64/$NAME

echo "remove temp files ..."
rm -r ./fyne-cross