##!/bin/sh

NAME="sentinel"
BASE="0.2"
# Get latest build
BUILD=$(<build_version.txt)
chrlen=${#BUILD}
if [ $chrlen = 0 ]
then
  BUILD=0
fi

echo "START BUILDING WITH FYNE-CROSS (https://github.com/fyne-io/fyne-cross) $BASE.$BUILD..."

# windows
echo "  * WINDOWS (amd64,386)"
fyne-cross windows -arch=amd64,386 -name=$NAME.exe
echo "    copy executables ..."
cp ./fyne-cross/bin/windows-386/$NAME.exe ./__build/windows-386/$NAME.exe
cp ./fyne-cross/bin/windows-amd64/$NAME.exe ./__build/windows-amd64/$NAME.exe

# linux
echo "  * LINUX (amd64,386,arm,arm64)"
fyne-cross linux -arch=amd64,386,arm,arm64 -name=$NAME
echo "    copy executables ..."
cp ./fyne-cross/bin/linux-386/$NAME ./__build/linux-386/$NAME
cp ./fyne-cross/bin/linux-amd64/$NAME ./__build/linux-amd64/$NAME
cp ./fyne-cross/bin/linux-arm/$NAME ./__build/linux-arm/$NAME
cp ./fyne-cross/bin/linux-arm64/$NAME ./__build/linux-arm64/$NAME

# mac
echo "  * DARWIN (arm64)"
fyne-cross darwin -arch=arm64 -app-build=1 -app-version="$BASE.$BUILD" -app-id=$NAME -name=$NAME
echo "    copy executables ..."
cp ./fyne-cross/dist/darwin-arm64/$NAME.app/Contents/MacOS/gg-app-$NAME ./__build/darwin-arm64/$NAME

echo "remove temp files ..."
rm -r ./fyne-cross
rm ./gg-app-sentinel