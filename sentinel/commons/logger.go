package commons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Logger struct {
	mode   string
	root   string
	logger *gg_log.Logger
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewLogger(mode string, logger interface{}, allowLogRotate bool) gg_log.ILogger {
	instance := new(Logger)
	instance.mode = mode
	instance.root = gg.Paths.WorkspacePath("logging")

	// reset file
	if !allowLogRotate {
		_ = gg.IO.RemoveAll(instance.root)
	}
	_ = gg.Paths.Mkdir(instance.root + gg_utils.OS_PATH_SEPARATOR)

	if instance.logger, _ = logger.(*gg_log.Logger); nil == instance.logger {
		instance.logger = gg_log.NewLogger()
		instance.logger.SetFilename(gg.Paths.Concat(instance.root, "logging.log"))
	}

	if mode == ModeDebug {
		instance.logger.SetLevel(gg_log.DebugLevel)
	} else {
		instance.logger.SetLevel(gg_log.InfoLevel)
	}

	instance.logger.RotateEnable(allowLogRotate)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Logger) String() string {
	if nil != instance {
		return instance.logger.String()
	}
	return ""
}

func (instance *Logger) RotateEnable(value bool) *Logger {
	if nil != instance {
		instance.logger.RotateEnable(value)
	}
	return instance
}

func (instance *Logger) RotateMaxSizeMb(value float64) *Logger {
	if nil != instance {
		instance.logger.RotateMaxSizeMb(value)
	}
	return instance
}

func (instance *Logger) GetFilename() string {
	if nil != instance {
		return instance.logger.GetFilename()
	}
	return ""
}

func (instance *Logger) SetFilename(filename string) *Logger {
	if nil != instance {
		instance.logger.SetFilename(filename)
	}
	return instance
}

func (instance *Logger) SetDateFormat(format string) *Logger {
	if nil != instance {
		instance.logger.SetDateFormat(format)
	}
	return instance
}

func (instance *Logger) GetDateFormat() (response string) {
	if nil != instance {
		response = instance.logger.GetDateFormat()
	}
	return
}

func (instance *Logger) Close() {
	instance.logger.Close()
}

func (instance *Logger) SetLevel(level string) *Logger {
	instance.logger.SetLevel(level)
	return instance
}

func (instance *Logger) GetLevel() gg_log.Level {
	return instance.logger.GetLevel()
}

func (instance *Logger) Panic(args ...interface{}) {
	instance.logger.Panic(args...)
}

func (instance *Logger) Trace(args ...interface{}) {
	instance.logger.Trace(args...)
}

func (instance *Logger) Debug(args ...interface{}) {
	// file logging
	instance.logger.Debug(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Info(args ...interface{}) {
	// file logging
	instance.logger.Info(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Error(args ...interface{}) {
	// file logging
	instance.logger.Error(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}

func (instance *Logger) Warn(args ...interface{}) {
	// file logging
	instance.logger.Warn(args...)

	if instance.mode == ModeDebug {
		// console logging
		fmt.Println(args...)
	}
}
