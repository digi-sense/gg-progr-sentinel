package commons

import "errors"

const (
	Version = "1.1.0"
	Name    = "APPLICATION LAUNCHER 🚀"

	ModeProduction = "production"
	ModeDebug      = "debug"

	DirStart = "start"
	DirApp   = "app"
	DirWork  = "*"

	TaskEventFileChanged = "file-changed" // do action if a file is changed
	TaskActionRestart    = "restart"
)

// ---------------------------------------------------------------------------------------------------------------------
//		e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	PanicSystemError          = errors.New("panic_system_error")
	InvalidConfigurationError = errors.New("invalid_configuration_error")
	UpdaterIsNotReadyError    = errors.New("updater_is_not_ready_error")
	MissingConfigurationError = errors.New("missing_configuration_error") // configuration file was created with some default sample data
)
