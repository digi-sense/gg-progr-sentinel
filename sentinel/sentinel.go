package sentinel

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_ticker"
	"bitbucket.org/digi-sense/gg-core/gg_updater"
	"bitbucket.org/digi-sense/gg-progr-sentinel/sentinel/commons"
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type Launcher struct {
	root       string
	dirStart   string
	dirApp     string
	dirWork    string
	stopDirs   []string
	settings   *commons.Settings
	logger     gg_log.ILogger
	updater    *gg_updater.Updater
	stopTicker *gg_ticker.Ticker
	chanQuit   chan bool
	mux        sync.Mutex
	fileHashes map[string]string
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewAppLauncher(dirStart, dirApp, dirWork, mode string) (*Launcher, error) {
	dirWork = gg.Paths.Absolute(dirWork)
	if len(dirStart) == 0 {
		dirStart = gg.Paths.Dir(dirWork)
	}
	if len(dirApp) == 0 {
		dirApp = gg.Paths.Dir(dirWork)
	}
	gg.Paths.GetWorkspace(commons.DirStart).SetPath(dirStart)
	gg.Paths.GetWorkspace(commons.DirApp).SetPath(dirApp)
	gg.Paths.SetWorkspacePath(dirWork)
	err := gg.Paths.Mkdir(gg.Paths.GetWorkspacePath() + string(os.PathSeparator))
	if nil != err {
		return nil, err
	}

	instance := new(Launcher)
	instance.fileHashes = make(map[string]string)
	instance.chanQuit = make(chan bool, 1)
	instance.dirStart = gg.Paths.GetWorkspace(commons.DirStart).GetPath()
	instance.dirApp = gg.Paths.GetWorkspace(commons.DirApp).GetPath()
	instance.dirWork = gg.Paths.GetWorkspacePath()
	instance.root = filepath.Dir(instance.dirWork)
	instance.stopDirs = []string{
		instance.root,
		instance.dirWork,
		instance.dirApp,
	}
	instance.logger = commons.NewLogger(mode, nil, true)

	err = instance.init()

	if nil == err {
		instance.logger.Debug(fmt.Sprintf("running with settings: \n %v", instance.settings.String()))
	}
	if nil != instance.settings {
		if !instance.settings.KeepAlive {
			instance.logger.Warn("⚠️ KEEP-ALIVE IS NOT ENABLED!")
		} else {
			instance.logger.Info("💪 KEEP-ALIVE IS ENABLED!")
		}
	}

	return instance, err
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Launcher) GetLogger() gg_log.ILogger {
	return instance.logger
}

func (instance *Launcher) IsRunning() bool {
	if nil != instance && nil != instance.stopTicker && nil != instance.updater {
		return instance.updater.IsProcessRunning()
	}
	return false
}

func (instance *Launcher) Restart() {
	if nil != instance && instance.IsRunning() {
		instance.updater.ReStart()
	}
}

func (instance *Launcher) Start() error {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			if nil != instance {
				instance.free()
				err := instance.init()
				if nil == err {
					// recovery start
					_ = instance.start()
				}
			}
		}
	}()
	// start
	return instance.start()
}

func (instance *Launcher) Wait() error {
	if nil != instance.updater {
		<-instance.chanQuit // wait exit
		instance.chanQuit = make(chan bool, 1)
	}
	return commons.UpdaterIsNotReadyError
}

func (instance *Launcher) Stop() {
	if nil != instance {
		if nil != instance.updater {
			instance.updater.Stop()
		}
		if nil != instance.stopTicker {
			instance.stopTicker.Stop()
		}

		cmd := instance.settings.StopCmd
		if len(cmd) > 0 {
			// file check
			if b, _ := gg.Paths.Exists(cmd); b {
				_ = gg.IO.Remove(cmd)
			}
		}

		instance.logger.Info("Stopped!")

		instance.chanQuit <- true
	}
}

// ----------------------------------------------------------------------------------------------------------------------
//
//	p r i v a t e
//
// ----------------------------------------------------------------------------------------------------------------------

func (instance *Launcher) free() {
	if nil != instance {
		if nil != instance.updater {
			instance.updater.Stop()
			instance.updater = nil
		}
		if nil != instance.stopTicker {
			instance.stopTicker.Stop()
			instance.stopTicker = nil
		}
	}
}

func (instance *Launcher) init() error {

	// detect configuration file
	config, err := instance.config()
	if nil != err {
		return err
	}

	instance.settings = config
	instance.updater = gg_updater.NewUpdater(config.String())
	instance.updater.SetRoot(instance.root)
	instance.updater.OnError(instance.onUpdaterError)
	instance.updater.OnUpgrade(instance.onUpdaterUpgrade)
	instance.updater.OnLaunchStart(instance.onLaunchStart)
	instance.updater.OnLaunchStarted(instance.onLaunchStarted)
	instance.updater.OnLaunchQuit(instance.onLaunchQuit)
	instance.updater.OnTask(instance.onTask)

	instance.stopTicker = gg_ticker.NewTicker(3*time.Second, func(t *gg_ticker.Ticker) {
		instance.checkStop()
		instance.logger.Debug("[DEBUG ONLY MESSAGE]", "Checking for stop command....")
	})

	return nil
}

func (instance *Launcher) start() error {
	if nil != instance {
		if nil != instance.updater {
			if nil != instance.stopTicker {
				instance.stopTicker.Start()
			}

			_, _, _, _, err := instance.updater.Start()

			return err
		}
	}
	return commons.UpdaterIsNotReadyError
}

func (instance *Launcher) config() (*commons.Settings, error) {
	var config *commons.Settings
	files, err := gg.Paths.ListFiles(instance.dirWork, "*.json")
	if nil != err {
		return nil, err
	}

	for _, file := range files {
		txt, err := gg.IO.ReadTextFromFile(file)
		if nil == err && len(txt) > 0 {
			config, err = commons.NewSettings(txt)
			if nil == err {
				if len(config.CommandToRun) > 0 {
					// found a valid configuration file
					return config, nil
				}
			}
		}
	}

	return nil, commons.InvalidConfigurationError // not found
}

func (instance *Launcher) onUpdaterError(err string) {
	if nil != instance && nil != instance.logger {
		instance.logger.Error(fmt.Sprintf("Updater Error: %v", err))
		instance.notify("error", fmt.Sprintf("ERROR: %s", err))
	}
}

func (instance *Launcher) onUpdaterUpgrade(fromVersion string, toVersion string, files []string) {
	if nil != instance && nil != instance.logger {
		instance.logger.Info(fmt.Sprintf("Updated from '%s' to '%s'.", fromVersion, toVersion))
		instance.notify("update", fmt.Sprintf("UPDATE: from version '%s' to version '%s' files=%v", fromVersion, toVersion, len(files)))
	}
}

func (instance *Launcher) onLaunchStart(command string) {
	if nil != instance && nil != instance.logger {
		instance.logger.Info(fmt.Sprintf("Starting exec '%s'.", command))
	}
}

func (instance *Launcher) onLaunchStarted(command string, pid int) {
	if nil != instance && nil != instance.logger {
		instance.logger.Info(fmt.Sprintf("Started exec '%s' (pid=%v).", command, pid))
		instance.notify("start", fmt.Sprintf("START: %s (pid=%v)", command, pid))
	}
}

func (instance *Launcher) onLaunchQuit(command string, pid int) {
	if nil != instance && nil != instance.logger {
		instance.logger.Info(fmt.Sprintf("Quitting exec '%s' pid %v.", command, pid))
		instance.notify("quit", fmt.Sprintf("QUIT: %s", command))
	}
}

func (instance *Launcher) onTask(uid string, payload map[string]interface{}) {
	if nil != instance && nil != payload {
		event := gg.Reflect.GetString(payload, "event")
		switch event {
		case commons.TaskEventFileChanged:
			filename := gg.Reflect.GetString(payload, "file")
			if instance.isFileChanged(filename) {
				action := gg.Reflect.GetString(payload, "action")
				switch action {
				case commons.TaskActionRestart:
					instance.Restart()
					instance.notify("task", fmt.Sprintf("TASK '%s' EXECUTED: %s", uid, gg.Convert.ToString(payload)))
				default:
					instance.Restart()
					instance.notify("task", fmt.Sprintf("TASK '%s' EXECUTED: %s", uid, gg.Convert.ToString(payload)))
				}
			}
		default:
			// not supported
		}
	}
}

func (instance *Launcher) checkStop() {
	if nil != instance && nil != instance.settings {
		instance.mux.Lock()
		defer instance.mux.Unlock()

		cmd := instance.settings.StopCmd
		if len(cmd) > 0 {
			// check if file exists
			for _, path := range instance.stopDirs {
				cmdFile := gg.Paths.Concat(path, cmd)
				if b, _ := gg.Paths.Exists(cmdFile); b {
					_ = gg.IO.Remove(cmdFile)
					instance.Stop()
					return
				}
			}
		}
	}
}

func (instance *Launcher) isFileChanged(filename string) bool {
	abs := gg.Paths.WorkspacePath(filename)
	hash, err := gg.IO.ReadHashFromFile(abs)
	if nil == err {
		if value, b := instance.fileHashes[abs]; b {
			instance.fileHashes[abs] = hash
			return value != hash
		} else {
			// just add file first time
			instance.fileHashes[abs] = hash
		}
	}
	return false
}

func (instance *Launcher) notify(event, message string) {
	if nil != instance && nil != instance.settings && nil != instance.settings.Notifications && len(message) > 0 {
		// SMS
		if nil != instance.settings.Notifications.Sms && len(instance.settings.Notifications.Sms) > 0 {
			to := gg.Reflect.GetString(instance.settings.Notifications.Sms, "to")
			from := gg.Reflect.GetString(instance.settings.Notifications.Sms, "from")
			if len(from) == 0 {
				from = "App Launcher"
			}
			if len(to) > 0 {
				sms := ggx.SMS.NewEngine(instance.settings.Notifications.Sms)
				if len(sms.ProviderNames()) > 0 {
					// READY TO SEND
					_, _ = sms.SendMessage("", message, to, from)
				}
			}
		}
		// EMAIL
		if nil != instance.settings.Notifications.Email && len(instance.settings.Notifications.Email) > 0 {
			to := gg.Reflect.GetString(instance.settings.Notifications.Email, "to")
			from := gg.Reflect.GetString(instance.settings.Notifications.Email, "from")
			if len(to) > 0 {
				smtp, err := gg.Email.NewSender(instance.settings.Notifications.Email)
				if nil == err {
					subject := fmt.Sprintf("%s (%s) - %s", commons.Name, commons.Version, event)
					_ = smtp.Send(subject, message, []string{to}, []string{}, []string{}, from, "", nil)
				}
			}
		}
	}
}
